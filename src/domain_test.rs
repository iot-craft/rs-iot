#[test]
fn test_bitwise_and() {
    use super::domain::bitwise_and;
    assert!(bitwise_and(&["1.000".to_string(), "1.000".to_string()]));
    assert!(!bitwise_and(&["1.000".to_string(), "0.000".to_string()]));
}

#[test]
fn test_average() {
    use super::domain::average;
    assert_eq!(
        average(&[
            "1.000".to_string(),
            "2.000".to_string(),
            "3.000".to_string()
        ]),
        2.0
    );
}
