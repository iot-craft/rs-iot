pub fn bitwise_and(children: &[String]) -> bool {
    children.iter().all(|x| x.parse::<f32>().unwrap() == 1.0)
}

pub fn average(children: &[String]) -> f32 {
    let sum: f32 = children.iter().map(|x| x.parse::<f32>().unwrap()).sum();
    sum / children.len() as f32
}
