FROM rust:1.66.0-slim-buster
WORKDIR /app
COPY cfg.yml .
COPY src src
COPY Cargo.* ./
RUN cargo install --path .
#ARG PORT
#ARG ENV
#ENV ENV=$ENV
#EXPOSE $PORT
CMD mqtt
