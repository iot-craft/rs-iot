# rs-iot 🦀

![](https://img.shields.io/gitlab/pipeline-status/iot-craft/rs-iot?branch=beta&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/gitlab/coverage/iot-craft/rs-iot/beta?logo=rust&style=for-the-badge)

## Pre-Requisites
- `rustup component add rustfmt`
- `rustup component add clippy`
- `cargo install cargo-cmd`
- `cargo install cargo-audit`
- `cargo install cargo-tarpaulin`
- `cargo install cargo-udeps --locked`
- `cargo install cargo2junit`